package com.paiwad.myapp.covide_trackapp.data.source

import com.paiwad.myapp.covide_trackapp.data.model.*

class FakeData {

    val countries = mutableListOf<LocalCountries>()
    val byCountry = mutableListOf<Country>()
    lateinit var summaryResponse: SummaryResponse

    init {
        initSummaryResponse()
        initByCountry()
        initCountries()
    }

    private fun initByCountry() {
        val country = Country(
            18204,
            "", "",
            45185,
            "", "",
            "2021-04-21T00:00:00Z",
            108,
            "", "", "", 26873
        )
        byCountry.add(country)
        byCountry.add(
            country.copy(
                date = "2021-04-22T00:00:00Z",
                confirmed = 46643,
                deaths = 110,
                recovered = 26873,
                active = 19660
            )
        )
    }

    private fun initSummaryResponse() {
        val countryList = listOf<CountryX>(
            CountryX("Zimbabwe",
                "ZW",
                "2021-04-21T17:18:41.981Z",
                0,
                37875,
                0,
                "zimbabwe",
                0,
                35058,
                35058
            )
        )
        summaryResponse = SummaryResponse(
            countryList,
            "2021-04-21T17:18:41.981Z",
            Global(
                563034,
                142747257,
                9444,
                3041677,
                359855,
                81885998,
                "2021-04-21T17:18:41.981Z"
            )
        )
    }

    private fun initCountries(){
        countries.add(LocalCountries("ALA Aland Islands", "ala-aland-islands", "AX"))
        countries.add(LocalCountries("Bahrain", "bahrain", "BM"))
        countries.add(LocalCountries("Cameroon", "cameroon", "CM"))
    }
}