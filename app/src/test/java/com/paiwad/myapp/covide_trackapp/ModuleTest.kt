package com.paiwad.myapp.covide_trackapp

import com.paiwad.myapp.covide_trackapp.data.FakeRepository
import com.paiwad.myapp.covide_trackapp.data.source.FakeData
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19LocalDataSource
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19RemoteDataSource
import com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl.Covid19LocalDataSourceImpl
import com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl.Covid19RemoteDataSourceImpl
import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import com.paiwad.myapp.covide_trackapp.viewmodel.loading.DefaultLoadingStateViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val fakeModule = module {
    single { FakeRepository() }
    single { FakeData() }
    viewModel { Covid19ViewModel(androidContext(),DefaultLoadingStateViewModel(), get() as FakeRepository) }

    single<Covid19LocalDataSource> { Covid19LocalDataSourceImpl(get()) }
    single<Covid19RemoteDataSource> { Covid19RemoteDataSourceImpl(get()) }
}