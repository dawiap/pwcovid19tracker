package com.paiwad.myapp.covide_trackapp.data

import android.content.Context
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.paiwad.myapp.covide_trackapp.MainCoroutineRule
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.Covid19RepositoryImpl
import com.paiwad.myapp.covide_trackapp.data.source.FakeData
import com.paiwad.myapp.covide_trackapp.data.source.FakeLocalDataSource
import com.paiwad.myapp.covide_trackapp.data.source.FakeRemoteDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okio.buffer
import okio.source
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import retrofit2.Response
import kotlinx.coroutines.test.runBlockingTest
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@Config(sdk = [Config.OLDEST_SDK])
@ExperimentalCoroutinesApi
class Covid19RepositoryTest {

    private lateinit var fakeData: FakeData

    private lateinit var localDataSource: FakeLocalDataSource
    private lateinit var remoteDataSource: FakeRemoteDataSource

    private var remoteSummaryResponse: SummaryResponse? = null
    private var remoteDataOfCountry = mutableListOf<Country>()
    private var remoteCountries = mutableListOf<LocalCountries>()
    private var localCountries = mutableListOf<LocalCountries>()

    private lateinit var repository: Covid19Repository

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createRepository() {
        fakeData = FakeData()

        localDataSource = FakeLocalDataSource(fakeData.countries)
        remoteDataSource = FakeRemoteDataSource(
            fakeData.summaryResponse,
            fakeData.countries,
            fakeData.byCountry
        )

        remoteSummaryResponse = fakeData.summaryResponse
        remoteDataOfCountry = fakeData.byCountry
        remoteCountries = fakeData.countries
        localCountries = fakeData.countries

        repository = Covid19RepositoryImpl(localDataSource,remoteDataSource)
    }

    @Test
    fun getSummary_requestSummaryFromRemoteDataSource() = mainCoroutineRule.runBlockingTest{
        //when summary are requested from the repository
        val summaryResponse = repository.getDataSummary()

        //then summary are loaded from the remote data source
        Truth.assertThat(summaryResponse.data).isEqualTo(remoteSummaryResponse)
    }

    @Test
    fun getDataByCountry_requestCountryFromRemoteDataSource() = mainCoroutineRule.runBlockingTest {
        //when data of country are request the repository
        val dataOfCountry = repository.getDataByCountry("Thailand")

        //then data of country are loaded from the remote data source
        Truth.assertThat(dataOfCountry.data).isEqualTo(remoteDataOfCountry)
    }

    @Test
    fun getCountries_requestCountriesFromRemoteDataSource() = mainCoroutineRule.runBlockingTest {
        //when countries are request from the repository
        val countries = repository.getCountries(true)

        //then countries are loaded from the local data source
        Truth.assertThat(countries.data).isEqualTo(remoteCountries)
    }

    @Test
    fun getCountries_requestCountriesFromLocalDataSource() = mainCoroutineRule.runBlockingTest {
        //when countries are request from the repository
        val countries = repository.getCountries(false)

        //then countries are loaded from the remote data source
        Truth.assertThat(countries.data).isEqualTo(localCountries)
    }
}