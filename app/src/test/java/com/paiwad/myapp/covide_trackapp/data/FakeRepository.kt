package com.paiwad.myapp.covide_trackapp.data

import com.example.myapplication.data.util.Resource
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.FakeData

class FakeRepository(
        val summaryResponse: SummaryResponse = FakeData().summaryResponse,
        val localCountries: List<LocalCountries> = FakeData().countries,
        val dataOfCountry: List<Country> = FakeData().byCountry
): Covid19Repository {
    override suspend fun getDataSummary(): Resource<SummaryResponse> {
        return Resource.Success(summaryResponse)
    }

    override suspend fun getDataByCountry(country: String): Resource<List<Country>> {
        return Resource.Success(dataOfCountry)
    }

    override suspend fun getCountries(isOnline: Boolean): Resource<List<LocalCountries>> {
        return Resource.Success(localCountries)
    }
}