package com.paiwad.myapp.covide_trackapp.data.source

import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19LocalDataSource

class FakeLocalDataSource(var countries: MutableList<LocalCountries>? = mutableListOf()): Covid19LocalDataSource {
    override suspend fun getCountryFromCatch(): List<LocalCountries> {
        countries?.let { return ArrayList(it) }
        return listOf()
    }

    override suspend fun addCountryToCatch(country: List<LocalCountries>) {
        countries?.addAll(country)
    }

    override suspend fun clearCountry() {
        countries?.clear()
    }
}