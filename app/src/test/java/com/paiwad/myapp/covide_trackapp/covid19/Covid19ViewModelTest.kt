package com.paiwad.myapp.covide_trackapp.covid19

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.paiwad.myapp.covide_trackapp.MainCoroutineRule
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.FakeData
import com.paiwad.myapp.covide_trackapp.fakeModule
import com.paiwad.myapp.covide_trackapp.getOrAwaitValue
import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject

@ExperimentalCoroutinesApi
class Covid19ViewModelTest: KoinTest {

    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val viewModel: Covid19ViewModel by inject()
    private val fakeData: FakeData by inject()

    private var fakeSummaryResponse: SummaryResponse? = null
    private var fakeCountries = mutableListOf<LocalCountries>()
    private var fakeDataOfCountry = mutableListOf<Country>()

    @Before
    fun setUp() {
       startKoin {
           androidContext(Application())
           modules(fakeModule)
       }

        fakeSummaryResponse = fakeData.summaryResponse
        fakeCountries = fakeData.countries
        fakeDataOfCountry = fakeData.byCountry
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test
    fun `get data summary with all, return summary`() {
        val summaryResponse = viewModel.getSummary().getOrAwaitValue()

        Truth.assertThat(summaryResponse.data).isEqualTo(fakeSummaryResponse)
    }

    @Test
    fun `get countries with all country, return countries`(){
        val countries = viewModel.getCountries().getOrAwaitValue()

        Truth.assertThat(countries.data).isEqualTo(fakeCountries)
    }

    @Test
    fun `get data country with selected country item, return country`() {
        val country = viewModel.getDataByCountry("Thailand").getOrAwaitValue()

        Truth.assertThat(country.data).isEqualTo(fakeDataOfCountry)
        Truth.assertThat(country.data?.size).isEqualTo(2)
    }
}