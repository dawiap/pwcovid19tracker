package com.paiwad.myapp.covide_trackapp.data.source

import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19RemoteDataSource
import retrofit2.Response

class FakeRemoteDataSource(
    val summary: SummaryResponse,
    val countries: List<LocalCountries>,
    val byCountry: List<Country>
): Covid19RemoteDataSource {
    override suspend fun getSummary(): Response<SummaryResponse> {
        return Response.success(summary)
    }

    override suspend fun getByCountry(country: String): Response<List<Country>> {
        return Response.success(byCountry)
    }

    override suspend fun getCountry(): Response<List<LocalCountries>> {
        return Response.success(countries)
    }
}