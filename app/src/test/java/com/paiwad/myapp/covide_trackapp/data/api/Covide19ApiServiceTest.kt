package com.paiwad.myapp.covide_trackapp.data.api

import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Covide19ApiServiceTest {

    private lateinit var api: Covide19ApiService
    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        api = Retrofit.Builder()
                .baseUrl(server.url(""))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Covide19ApiService::class.java)
    }

    @Test
    fun `get countries retrieves expected`() = runBlocking{
        //give
        enqueueMockResponse("countriesresponse.json")

        //when
        val responseBody = api.getCountries().body()
        val request = server.takeRequest()

        //then
        Truth.assertThat(responseBody).isNotNull()
        Truth.assertThat(request.path).isEqualTo("/countries")
    }


    @Test
    fun `get countries retrieves expected thailand data`() = runBlocking {
        //give
        enqueueMockResponse("countriesresponse.json")
        val _country = "Thailand"

        //when
        val responseBody = api.getCountries().body()
        val thailand = responseBody!!.find { it.country == _country }

        //then
        Truth.assertThat(thailand).isNotNull()
        Truth.assertThat(thailand?.country).isEqualTo(_country)
    }

    @Test
    fun `get live data from thailand retrieves expected data`() = runBlocking{
        //give
        enqueueMockResponse("livethailandresponse.json")
        val _country = "Thailand"

        //when
        val responseBody = api.getTotalByCountry(_country).body()
        val request = server.takeRequest()

        //then
        Truth.assertThat(responseBody).isNotNull()
        Truth.assertThat(request.path).isEqualTo("/live/country/$_country")
    }

    @Test
    fun `get live data with thailand item request retrieves expected thailand data`() = runBlocking {
        //give
        enqueueMockResponse("livethailandresponse.json")
        val _country = "Thailand"

        //when
        val responseBody = api.getTotalByCountry(_country).body()
        val thailand = responseBody!!.last()

        //then
        Truth.assertThat(thailand.date).isEqualTo("2021-04-22T00:00:00Z")
        Truth.assertThat(thailand.confirmed).isEqualTo(46643)
        Truth.assertThat(thailand.active).isEqualTo(19660)
        Truth.assertThat(thailand.recovered).isEqualTo(26873)
        Truth.assertThat(thailand.deaths).isEqualTo(110)
    }

    @Test
    fun `get summary retrieves expected data`() = runBlocking{
        //give
        enqueueMockResponse("summaryresponse.json")

        //when
        val responseBody = api.getSummary().body()
        val request = server.takeRequest()

        //then
        Truth.assertThat(responseBody).isNotNull()
        Truth.assertThat(request.path).isEqualTo("/summary")
    }

    @Test
    fun `get summary retrieves expected global data`() = runBlocking {
        //give
        enqueueMockResponse("summaryresponse.json")

        //when
        val responseBody = api.getSummary().body()
        val global = responseBody!!.global

        //then
        Truth.assertThat(global.Date).isEqualTo("2021-04-21T17:18:41.981Z")
        Truth.assertThat(global.newConfirmed).isEqualTo(563034)
        Truth.assertThat(global.newDeaths).isEqualTo(9444)
        Truth.assertThat(global.newRecovered).isEqualTo(359855)
    }

    @Test
    fun `get summary retrieves expected thailand data`() = runBlocking{
        //give
        enqueueMockResponse("summaryresponse.json")

        //when
        val responseBody = api.getSummary().body()
        val countries = responseBody!!.countries
        val isThailand = countries.find { it.country == "Thailand" }

        //then
        Truth.assertThat(isThailand).isNotNull()
    }

    private fun enqueueMockResponse(_file: String){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(_file)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }
}