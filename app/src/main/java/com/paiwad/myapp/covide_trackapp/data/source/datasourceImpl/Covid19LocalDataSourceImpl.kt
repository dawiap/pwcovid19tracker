package com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl

import com.paiwad.myapp.covide_trackapp.data.db.Covid19Dao
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19LocalDataSource
import kotlinx.coroutines.*

class Covid19LocalDataSourceImpl(private val dao: Covid19Dao): Covid19LocalDataSource {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
    override suspend fun getCountryFromCatch(): List<LocalCountries> {
        return withContext(ioDispatcher){
            dao.getCountry()
        }
    }

    override suspend fun addCountryToCatch(country: List<LocalCountries>) {
        return withContext(ioDispatcher) {
            dao.addCountry(country)
        }
    }

    override suspend fun clearCountry() {
        return withContext(ioDispatcher) {
            dao.clearCountry()
        }
    }
}