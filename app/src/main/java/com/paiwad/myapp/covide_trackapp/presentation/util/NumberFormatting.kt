package com.paiwad.myapp.covide_trackapp.presentation.util

import java.text.DecimalFormat
import java.time.temporal.TemporalAmount

class NumberFormatting {
    fun format(amount: Double): String {
        return DecimalFormat("#,###").format(amount)
    }
}