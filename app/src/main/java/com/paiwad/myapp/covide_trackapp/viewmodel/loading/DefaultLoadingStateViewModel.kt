package com.paiwad.myapp.covide_trackapp.viewmodel.loading

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paiwad.myapp.covide_trackapp.viewmodel.singleEvent.SingleEvent

class DefaultLoadingStateViewModel: ViewModel(), LoadingStateViewModel {
    private val _loadingState = MutableLiveData<SingleEvent<Boolean>>()
    override val loadingState: LiveData<SingleEvent<Boolean>>
        get() = _loadingState

    override fun showLoading() {
        _loadingState.postValue(SingleEvent(true))
    }

    override fun hideLoading() {
        _loadingState.postValue(SingleEvent(false))
    }
}