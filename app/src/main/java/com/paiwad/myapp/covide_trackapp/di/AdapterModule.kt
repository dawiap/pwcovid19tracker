package com.paiwad.myapp.covide_trackapp.di

import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.presentation.adapter.*
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting
import org.koin.dsl.module

val adapterModule = module {
    single { (items: List<LocalCountries>) -> CustomDropDownAdapter(items) }
    single { (listener: (CountryX) -> Unit,fm: NumberFormatting) -> RankListGridAdapter(listener, fm) }
    single { (fm: NumberFormatting) -> RankListHorizentralAdapter(fm) }
    single { (adapter: RankListHorizentralAdapter) -> HorizentralWrapperAdapter(adapter) }
    single { (fm: NumberFormatting) -> RankListVerticalAdapter(fm) }
}