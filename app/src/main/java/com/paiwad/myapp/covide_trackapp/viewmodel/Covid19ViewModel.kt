package com.paiwad.myapp.covide_trackapp.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.*
import com.paiwad.myapp.covide_trackapp.data.Covid19Repository
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.viewmodel.loading.LoadingStateViewModel
import kotlinx.coroutines.flow.flow

class Covid19ViewModel(
        val context: Context,
    val loadingStateViewModel: LoadingStateViewModel,
    private val repository: Covid19Repository,
    ): ViewModel() {

    val _rank = MutableLiveData<List<CountryX>>()
    val rank: LiveData<List<CountryX>> = _rank

    fun getDataByCountry(country: String) = liveData {
        val data = repository.getDataByCountry(country)
        emit(data)
    }

    fun getCountries() = liveData {
        val result = repository.getCountries(isNetworkAvailable())
        emit(result)
    }

    fun getSummary() = liveData {
        val result = repository.getDataSummary()
        emit(result)
    }

    fun isNetworkAvailable(): Boolean {
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            connectivityManager?.run {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                    result = when {
                        hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                        else -> false
                    }
                }
            }
        } else {
            connectivityManager?.run {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    return true
                }
            }
        }
        return result
    }
}