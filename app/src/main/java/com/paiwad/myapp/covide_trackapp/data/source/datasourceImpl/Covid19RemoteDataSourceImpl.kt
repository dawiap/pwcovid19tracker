package com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl

import com.paiwad.myapp.covide_trackapp.data.api.Covide19ApiService
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19RemoteDataSource
import retrofit2.Response

class Covid19RemoteDataSourceImpl(private val apiService: Covide19ApiService): Covid19RemoteDataSource {
    override suspend fun getSummary(): Response<SummaryResponse> {
        return apiService.getSummary()
    }

    override suspend fun getByCountry(country: String): Response<List<Country>> {
        return apiService.getTotalByCountry(country)
    }

    override suspend fun getCountry(): Response<List<LocalCountries>> {
        return apiService.getCountries()
    }
}