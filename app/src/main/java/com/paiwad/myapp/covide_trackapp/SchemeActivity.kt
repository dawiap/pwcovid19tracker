package com.paiwad.myapp.covide_trackapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.paiwad.myapp.covide_trackapp.presentation.MainActivity

class SchemeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val uri = intent.data
        uri?.let {
            val page = it.getQueryParameter("activity")
            val frag = it.getQueryParameter("frag")
            Log.i("PAGE","activity: "+page.toString())
            if(page?.toInt() == 1){
                val intent = Intent(this,MainActivity::class.java)
                frag?.let {
                    intent.putExtra("frag",frag.toInt())
                }
                startActivity(intent)
            }
        }
    }
}