package com.paiwad.myapp.covide_trackapp.di

import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19RemoteDataSource
import com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl.Covid19RemoteDataSourceImpl
import org.koin.dsl.module

val remoteDataModule = module {
    single<Covid19RemoteDataSource> { Covid19RemoteDataSourceImpl(get()) }
}