package com.paiwad.myapp.covide_trackapp

import android.app.Application
import com.paiwad.myapp.covide_trackapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class Covid19Application: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@Covid19Application)
            modules(listOf(
                databaseModule,
                networkModule,
                localDataModule,
                remoteDataModule,
                repositoryModule,
                viewModelModule,
                adapterModule,
                utilModule
            ))
        }
    }
}