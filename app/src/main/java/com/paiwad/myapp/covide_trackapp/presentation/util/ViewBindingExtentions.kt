package com.paiwad.myapp.covide_trackapp.presentation.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.presentation.adapter.RankListGridAdapter

@BindingAdapter("app:url")
fun setImage(imageView: ImageView, url: String){
    Glide.with(imageView.context)
            .load(url)
            .into(imageView)
}

@BindingAdapter("app:items")
fun setItems(rcv: RecyclerView, items: List<CountryX>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let {
        (rcv.adapter as RankListGridAdapter).apply {
            differ.submitList(it)
        }
    }
}