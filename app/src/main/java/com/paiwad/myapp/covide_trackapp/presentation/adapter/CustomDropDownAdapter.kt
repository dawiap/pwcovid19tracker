package com.paiwad.myapp.covide_trackapp.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.databinding.CustomSpinnerItemBinding

class CustomDropDownAdapter(private val dataSource: List<LocalCountries>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CustomSpinnerItemBinding.inflate(layoutInflater,parent,false)
        val vh = ItemHolder(binding)
        vh.bind(dataSource[position])

        return binding.root
    }

    override fun getItem(position: Int): Any {
        return dataSource[position];
    }

    override fun getCount(): Int {
        return dataSource.size;
    }

    override fun getItemId(position: Int): Long {
        return position.toLong();
    }

    private class ItemHolder(val binding: CustomSpinnerItemBinding) {
        fun bind(item: LocalCountries){
            binding.countries = item
            binding.executePendingBindings()
        }
    }

}