package com.paiwad.myapp.covide_trackapp.data.model


import com.google.gson.annotations.SerializedName

data class SummaryResponse(
    @SerializedName("Countries")
    val countries: List<CountryX>,
    @SerializedName("Date")
    val date: String,
    @SerializedName("Global")
    val global: Global
)