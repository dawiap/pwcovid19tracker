package com.paiwad.myapp.covide_trackapp.di

import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19LocalDataSource
import com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl.Covid19LocalDataSourceImpl
import org.koin.dsl.module

val localDataModule = module {
    single<Covid19LocalDataSource> { Covid19LocalDataSourceImpl(get()) }
}