package com.paiwad.myapp.covide_trackapp.data.source.datasource

import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries

interface Covid19LocalDataSource {
    suspend fun getCountryFromCatch(): List<LocalCountries>
    suspend fun addCountryToCatch(country: List<LocalCountries>)
    suspend fun clearCountry()
}