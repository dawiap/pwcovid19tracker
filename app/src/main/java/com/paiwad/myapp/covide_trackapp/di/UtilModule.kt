package com.paiwad.myapp.covide_trackapp.di

import android.app.Activity
import com.paiwad.myapp.covide_trackapp.presentation.util.MyProgressDialog
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val utilModule = module {
    single { NumberFormatting() }
    single { (context: Activity) -> MyProgressDialog(context) }
}