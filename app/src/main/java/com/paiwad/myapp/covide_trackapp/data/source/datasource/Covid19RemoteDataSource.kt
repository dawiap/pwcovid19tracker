package com.paiwad.myapp.covide_trackapp.data.source.datasource

import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import retrofit2.Response

interface Covid19RemoteDataSource {
    suspend fun getSummary(): Response<SummaryResponse>
    suspend fun getByCountry(country: String): Response<List<Country>>
    suspend fun getCountry(): Response<List<LocalCountries>>
}