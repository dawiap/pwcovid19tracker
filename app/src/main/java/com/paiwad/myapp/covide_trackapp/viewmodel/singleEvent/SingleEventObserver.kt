package com.paiwad.myapp.covide_trackapp.viewmodel.singleEvent

import androidx.lifecycle.Observer

class SingleEventObserver<T>(private val onEventUnconsumedContent: (T) -> Unit) :
    Observer<SingleEvent<T>> {
    override fun onChanged(event: SingleEvent<T>?) {
        event?.consume()?.run(onEventUnconsumedContent)
    }
}