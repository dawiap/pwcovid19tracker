package com.paiwad.myapp.covide_trackapp.di

import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import com.paiwad.myapp.covide_trackapp.viewmodel.loading.DefaultLoadingStateViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val viewModelModule = module {
    single { Covid19ViewModel(androidContext(),DefaultLoadingStateViewModel(),get()) }
}