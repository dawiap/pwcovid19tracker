package com.paiwad.myapp.covide_trackapp.data

import com.example.myapplication.data.util.Resource
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.Global
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse

interface Covid19Repository {
    suspend fun getDataSummary(): Resource<SummaryResponse>
    suspend fun getDataByCountry(country: String): Resource<List<Country>>
    suspend fun getCountries(isOnline: Boolean): Resource<List<LocalCountries>>
}