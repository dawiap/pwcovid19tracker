package com.paiwad.myapp.covide_trackapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.databinding.CustomRankVerticalItemBinding
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting

class RankListVerticalAdapter(
    private val formatting: NumberFormatting
): RecyclerView.Adapter<RankListVerticalAdapter.GlobalItemViewHolder>() {

    private var data: List<CountryX> = emptyList()
    var VIEW_TYPE: Int = 11

    class GlobalItemViewHolder(private val binding: CustomRankVerticalItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int, item: CountryX, formatting: NumberFormatting) {
            binding.countryX = item
            binding.formatt = formatting
            binding.tvNumber.text = (position + 1).toString()
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GlobalItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CustomRankVerticalItemBinding.inflate(layoutInflater, parent, false)
        return GlobalItemViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: GlobalItemViewHolder, position: Int) {
        val item = data.get(position)
        holder.bind(position, item, formatting)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE
    }

    fun updateData(data: List<CountryX>) {
        this.data = data
        notifyDataSetChanged()
    }
}