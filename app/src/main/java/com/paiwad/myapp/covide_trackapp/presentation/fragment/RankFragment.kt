package com.paiwad.myapp.covide_trackapp.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import com.paiwad.myapp.covide_trackapp.R
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.databinding.FragmentRankBinding
import com.paiwad.myapp.covide_trackapp.presentation.MainActivity
import com.paiwad.myapp.covide_trackapp.presentation.adapter.HorizentralWrapperAdapter
import com.paiwad.myapp.covide_trackapp.presentation.adapter.RankListVerticalAdapter
import com.paiwad.myapp.covide_trackapp.presentation.adapter.RankListGridAdapter
import com.paiwad.myapp.covide_trackapp.presentation.adapter.RankListHorizentralAdapter
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting
import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import org.koin.android.ext.android.get
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class RankFragment : Fragment() {

    private lateinit var binding: FragmentRankBinding
    private lateinit var firstVerticalAdapter: RankListVerticalAdapter
    private lateinit var secondHorizentralAdapter: RankListHorizentralAdapter
    private lateinit var horizentalWrapperAdapter: HorizentralWrapperAdapter
    private lateinit var thirdGridAdapter: RankListGridAdapter
    private val viewModel: Covid19ViewModel by viewModel()
    private lateinit var formatt: NumberFormatting
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.run {
            horizentalWrapperAdapter.onRestoreState(savedInstanceState)
        }

        binding = FragmentRankBinding.bind(view)
        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.viewModel = viewModel

        formatt = (activity as MainActivity).fomatter

        setInitAdapter()
        setupListAdapter()
    }

    private fun setInitAdapter(){
        firstVerticalAdapter = get { parametersOf(formatt) }

        secondHorizentralAdapter = get { parametersOf(formatt) }

        thirdGridAdapter = get {
            parametersOf(
                { selectedCountryItem: CountryX -> listItemClicked(selectedCountryItem) },
                formatt
            )
        }
    }

    private fun getDataTop5(countryX: List<CountryX>): List<CountryX>{
        val sorted = countryX.sortedBy { it.totalConfirmed }.reversed()
        val top5 = mutableListOf<CountryX>()
        for(i in 0..2){
            top5.add(sorted.get(i))
        }
        return top5
    }

    private fun getNewConfirmed(countryX: List<CountryX>): List<CountryX>{
        val sorted = countryX.sortedBy { it.newConfirmed }.reversed()
        val top10 = mutableListOf<CountryX>()
        for(i in 0..9){
            top10.add(sorted.get(i))
        }
        return top10
    }

    private fun setupListAdapter() {

        viewModel.getSummary().observe(requireActivity(),{
            it.data?.let { summary ->

                firstVerticalAdapter.updateData(getDataTop5(summary.countries))

                secondHorizentralAdapter.differ.submitList(getNewConfirmed(summary.countries))
                horizentalWrapperAdapter = get { parametersOf(secondHorizentralAdapter) }

                thirdGridAdapter.differ.submitList(summary.countries.sortedBy { it.country })

                val config = ConcatAdapter.Config.Builder().apply {
                    setIsolateViewTypes(false)
                }.build()

                val concatAdapter = ConcatAdapter(config,firstVerticalAdapter,horizentalWrapperAdapter,thirdGridAdapter)

                //val itemDecor = SimpleItemDecor(binding.rcvNews.context)
                //binding.rcvNews.addItemDecoration(itemDecor)

                val layoutManager = GridLayoutManager(requireContext(), 2)
                layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return when (concatAdapter.getItemViewType(position)) {
                            firstVerticalAdapter.VIEW_TYPE -> 2
                            thirdGridAdapter.VIEW_TYPE -> 1
                            horizentalWrapperAdapter.VIEW_TYPE -> 2
                            else -> 2
                        }
                    }
                }
                binding.rcvNews.layoutManager = layoutManager
                binding.rcvNews.adapter = concatAdapter
            }
        })
    }

    //item click
    private fun listItemClicked(country: CountryX) {
        //Toast.makeText(requireContext(), country.country, Toast.LENGTH_SHORT).show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        println("onSaveInstanceState")
        horizentalWrapperAdapter.onSaveState(outState)
    }
}