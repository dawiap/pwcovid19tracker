package com.paiwad.myapp.covide_trackapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.paiwad.myapp.covide_trackapp.data.model.*

@Database(entities = arrayOf(LocalCountries::class),version = 1,exportSchema = false)
abstract class CovidTrackingDatabase: RoomDatabase() {
    abstract fun covid19Dao(): Covid19Dao
}