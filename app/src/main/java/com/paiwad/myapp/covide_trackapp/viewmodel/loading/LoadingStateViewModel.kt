package com.paiwad.myapp.covide_trackapp.viewmodel.loading

import androidx.lifecycle.LiveData
import com.paiwad.myapp.covide_trackapp.viewmodel.singleEvent.SingleEvent

interface LoadingStateViewModel {
    val loadingState: LiveData<SingleEvent<Boolean>>
    fun showLoading()
    fun hideLoading()
}