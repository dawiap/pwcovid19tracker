package com.paiwad.myapp.covide_trackapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.databinding.CustomRankHorizentralItemBinding
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting

class RankListHorizentralAdapter(
        private val formatting: NumberFormatting
        ): RecyclerView.Adapter<RankListHorizentralAdapter.RankHorizentralItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankHorizentralItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CustomRankHorizentralItemBinding.inflate(layoutInflater, parent, false)
        return RankHorizentralItemViewHolder(
                binding
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: RankHorizentralItemViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.bind(item, formatting)
    }

    private val callBack = object : DiffUtil.ItemCallback<CountryX>() {
        override fun areItemsTheSame(oldItem: CountryX, newItem: CountryX): Boolean {
            return oldItem.country == newItem.country
        }

        override fun areContentsTheSame(oldItem: CountryX, newItem: CountryX): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, callBack)

    class RankHorizentralItemViewHolder(private val binding: CustomRankHorizentralItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CountryX, formatting: NumberFormatting) {
            binding.countryX = item
            binding.formatt = formatting
            binding.executePendingBindings()
        }
    }
}

