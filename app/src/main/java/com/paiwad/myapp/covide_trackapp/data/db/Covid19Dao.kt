package com.paiwad.myapp.covide_trackapp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.paiwad.myapp.covide_trackapp.data.model.Global
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries

@Dao
interface Covid19Dao {
    @Query("SELECT *FROM mas_country")
    suspend fun getCountry(): List<LocalCountries>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCountry(country: List<LocalCountries>)

    @Query("DELETE FROM mas_country")
    suspend fun clearCountry()
}