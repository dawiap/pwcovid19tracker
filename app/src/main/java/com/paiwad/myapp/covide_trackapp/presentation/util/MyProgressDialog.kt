package com.paiwad.myapp.covide_trackapp.presentation.util

import android.app.Activity
import android.app.Application
import android.app.ProgressDialog
import android.content.Context

class MyProgressDialog(context: Activity) {
    var progressDialog: ProgressDialog
    init {
        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading")
        progressDialog.setMessage("please wait...")
    }
    fun dismiss(){
        progressDialog.dismiss()
    }
    fun show(){
        progressDialog.show()
    }
}