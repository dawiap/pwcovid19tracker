package com.paiwad.myapp.covide_trackapp.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.databinding.CustomRankGridItemBinding
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting

class RankListGridAdapter(
        private val clickListener: (CountryX)-> Unit,
        private val formatting: NumberFormatting
        ): RecyclerView.Adapter<RankListGridAdapter.RankItemViewHolder>() {

    var VIEW_TYPE: Int = 22

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CustomRankGridItemBinding.inflate(layoutInflater, parent, false)
        return RankItemViewHolder(
                binding
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: RankItemViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.bind(item, formatting,clickListener)
    }

    override fun getItemViewType(position: Int): Int {
        return VIEW_TYPE
    }

    private val callBack = object : DiffUtil.ItemCallback<CountryX>() {
        override fun areItemsTheSame(oldItem: CountryX, newItem: CountryX): Boolean {
            return oldItem.country == newItem.country
        }

        override fun areContentsTheSame(oldItem: CountryX, newItem: CountryX): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, callBack)

    class RankItemViewHolder(private val binding: CustomRankGridItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CountryX, formatting: NumberFormatting,clickListener: (CountryX) -> Unit) {
            binding.countryX = item
            binding.formatt = formatting
            binding.executePendingBindings()

            binding.root.setOnClickListener {
                clickListener(item)
            }
        }
    }
}

