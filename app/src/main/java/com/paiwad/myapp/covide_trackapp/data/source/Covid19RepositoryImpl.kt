package com.paiwad.myapp.covide_trackapp.data.source

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import com.example.myapplication.data.util.Resource
import com.paiwad.myapp.covide_trackapp.data.Covid19Repository
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19LocalDataSource
import com.paiwad.myapp.covide_trackapp.data.source.datasource.Covid19RemoteDataSource
import java.util.concurrent.TimeUnit

class Covid19RepositoryImpl(
        private val localDataSource: Covid19LocalDataSource,
        private val remoteDataSource: Covid19RemoteDataSource
): Covid19Repository {
    override suspend fun getDataSummary(): Resource<SummaryResponse> {
        //return if (isNetworkAvailable()) {
            return try {
                val response = remoteDataSource.getSummary()
                if (response.isSuccessful) {
                    Resource.Success(response.body())
                } else {
                    Resource.Error(response.message())
                }
            } catch (e: Exception) {
                Resource.Error(e.message.toString())
            }
       /* } else {
            Resource.Error(CONNECTION_STATE)
        }*/
    }

    override suspend fun getDataByCountry(country: String): Resource<List<Country>> {
        //return if (isNetworkAvailable()) {
            return try {
                val response = remoteDataSource.getByCountry(country)
                if (response.isSuccessful) {
                    Resource.Success(response.body())
                } else {
                    Resource.Error(response.message())
                }
            } catch (e: Exception) {
                println(e.localizedMessage)
                Resource.Error(e.message.toString())
            }
       /* } else {
            Resource.Error(CONNECTION_STATE)
        }*/
    }

    override suspend fun getCountries(isOnline: Boolean): Resource<List<LocalCountries>> {
        if (isOnline) {
            val cached = localDataSource.getCountryFromCatch()
            return if (cached.isNullOrEmpty()) {
                val response = remoteDataSource.getCountry()
                if (response.isSuccessful) {
                    response.body()?.let {
                        Log.i("reponse", "A")
                        Resource.Success(it)
                    } ?: run {
                        Log.i("reponse", "B")
                        Resource.Error("No Data")
                    }
                } else {
                    Resource.Error(response.message())
                }
            }else {
                Log.i("reponse", "C")
                Resource.Success(cached)
            }
        }
        return Resource.Success(localDataSource.getCountryFromCatch())
    }

    companion object {
        const val CONNECTION_STATE = "No internet connection."
        val FRESH_TIMEOUT = TimeUnit.DAYS.toMillis(1)
    }
}