package com.paiwad.myapp.covide_trackapp.presentation

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.paiwad.myapp.covide_trackapp.R
import com.paiwad.myapp.covide_trackapp.databinding.ActivityMainBinding
import com.paiwad.myapp.covide_trackapp.presentation.fragment.HomeFragment
import com.paiwad.myapp.covide_trackapp.presentation.fragment.RankFragment
import com.paiwad.myapp.covide_trackapp.presentation.util.MyProgressDialog
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting
import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity() {
    val viewModel: Covid19ViewModel by viewModel()
    val fomatter: NumberFormatting by inject()
    //lateinit var progressDialog: MyProgressDialog
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this

        //progressDialog = get { parametersOf(this@MainActivity) }

        intent.extras?.let { it ->
            Log.i("PAGE", "frag: " + it.getInt("frag"))
            if(it.getInt("frag")==2){
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, RankFragment())
                    .commit()
            }else{
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, HomeFragment())
                    .commit()
            }
        }

        val navController = findNavController(R.id.fragment)
        binding.btnNavView.setupWithNavController(navController)
    }
}