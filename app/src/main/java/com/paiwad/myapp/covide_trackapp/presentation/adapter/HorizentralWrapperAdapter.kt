package com.paiwad.myapp.covide_trackapp.presentation.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.covide_trackapp.data.model.CountryX
import com.paiwad.myapp.covide_trackapp.databinding.CustomHorizentralWrapperBinding
import com.paiwad.myapp.covide_trackapp.databinding.CustomRankGridItemBinding
import com.paiwad.myapp.covide_trackapp.databinding.CustomRankVerticalItemBinding
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting

class HorizentralWrapperAdapter(
    private val adapter: RankListHorizentralAdapter
): RecyclerView.Adapter<HorizentralWrapperAdapter.HorizentralWrapperViewHolder>() {

    private var lastScrollX = 0
    var VIEW_TYPE: Int = 33

    companion object {
        private const val KEY_SCROLL_X = "horizontal.wrapper.adapter.key_scroll_x"
    }

    class HorizentralWrapperViewHolder(private val binding: CustomHorizentralWrapperBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(adapter: RankListHorizentralAdapter, lastScrollX: Int, onScrolled: (Int) -> Unit) {
            val context = binding.root.context
            binding.recyclerView.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
            binding.recyclerView.adapter = adapter
            binding.recyclerView.doOnPreDraw {
                binding.recyclerView.scrollBy(lastScrollX, 0)
            }
            binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    onScrolled(recyclerView.computeHorizontalScrollOffset())
                }
            })
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HorizentralWrapperViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CustomHorizentralWrapperBinding.inflate(layoutInflater, parent, false)
        return HorizentralWrapperViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: HorizentralWrapperViewHolder, position: Int) {
        holder.bind(adapter,lastScrollX){

        }
    }

    override fun getItemCount(): Int = 1

    fun onSaveState(outState: Bundle) {
        outState.putInt(KEY_SCROLL_X, lastScrollX)
    }

    fun onRestoreState(savedState: Bundle) {
        lastScrollX = savedState.getInt(KEY_SCROLL_X)
    }
}