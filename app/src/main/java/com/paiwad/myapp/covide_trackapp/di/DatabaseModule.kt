package com.paiwad.myapp.covide_trackapp.di

import android.app.Application
import androidx.room.Room
import com.paiwad.myapp.covide_trackapp.data.db.Covid19Dao
import com.paiwad.myapp.covide_trackapp.data.db.CovidTrackingDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    fun provideCovide19TrackingDatabase(context: Application): CovidTrackingDatabase{
        return Room.databaseBuilder(context, CovidTrackingDatabase::class.java,"covidtrackingDB")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideCovid19Dao(covidTrackingDatabase: CovidTrackingDatabase): Covid19Dao{
        return covidTrackingDatabase.covid19Dao()
    }

    single { provideCovide19TrackingDatabase(androidApplication()) }
    single { provideCovid19Dao(get()) }
}