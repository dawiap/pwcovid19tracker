package com.paiwad.myapp.covide_trackapp.data.api

import com.example.myapplication.data.util.Resource
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Covide19ApiService {
    @GET("/summary")
    suspend fun getSummary(): Response<SummaryResponse>

    @GET("/live/country/{country}")
    suspend fun getTotalByCountry(@Path("country")country: String): Response<List<Country>>

    @GET("/countries")
    suspend fun getCountries(): Response<List<LocalCountries>>

    @GET("/countries")
    suspend fun getCountriesTest(): Resource<List<LocalCountries>>
}