package com.paiwad.myapp.covide_trackapp.presentation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import com.paiwad.myapp.covide_trackapp.R
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse
import com.paiwad.myapp.covide_trackapp.databinding.FragmentHomeBinding
import com.paiwad.myapp.covide_trackapp.presentation.MainActivity
import com.paiwad.myapp.covide_trackapp.presentation.adapter.CustomDropDownAdapter
import com.paiwad.myapp.covide_trackapp.presentation.util.MyProgressDialog
import com.paiwad.myapp.covide_trackapp.presentation.util.NumberFormatting
import com.paiwad.myapp.covide_trackapp.viewmodel.Covid19ViewModel
import com.paiwad.myapp.covide_trackapp.viewmodel.singleEvent.SingleEventObserver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.get
import org.koin.core.parameter.parametersOf

class HomeFragment : Fragment() {
    private lateinit var viewModel: Covid19ViewModel
    private lateinit var binding: FragmentHomeBinding
    private lateinit var fomatter: NumberFormatting
    private lateinit var progressDialog: MyProgressDialog
    private lateinit var adapter: CustomDropDownAdapter
    private val COUNTRY = "thailand"
    private val COUNTRY_CODE = "TH"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)

        viewModel = (activity as MainActivity).viewModel
        fomatter = (activity as MainActivity).fomatter

        //progressDialog = (activity as MainActivity).progressDialog
        progressDialog = get { parametersOf(requireActivity()) }

        binding.formatt = fomatter
        binding.lifecycleOwner = this.viewLifecycleOwner

        setupObservers1()
        setupSpinnerItemSelected()
    }
    private fun setupObservers1(){
        lifecycleScope.launch {
            whenStarted {
                viewModel.loadingStateViewModel.showLoading()

                viewModel.getDataByCountry(COUNTRY).observe(viewLifecycleOwner,{resource ->
                    resource.data?.let {countryList ->
                        binding.countryistoday = countryList.last()
                        binding.countryisyesterday = countryList.get(countryList.size - 2)
                    } ?: setupTextDefaultNoData()
                })

                viewModel.getSummary().observe(viewLifecycleOwner,{resource ->
                    resource.data?.let {summary ->
                        val ranks = summary.countries.sortedBy { it.newConfirmed+it.totalConfirmed }.reversed()
                        viewModel._rank.postValue(ranks)
                        binding.global = summary.global
                    }
                })

                withContext(Dispatchers.Main){
                    viewModel.getCountries().observe(viewLifecycleOwner,{resource ->
                        resource.data?.let {countries ->
                            setupSpinnerDefaultItem(countries)
                        }
                        //viewModel.loadingStateViewModel.hideLoading()
                    })
                }
            }
        }

        //subsribe loading
        viewModel.loadingStateViewModel.loadingState.observe(viewLifecycleOwner,SingleEventObserver{
            if(it){
                progressDialog.show()
            }else{
                progressDialog.dismiss()
            }
        })
    }

    private fun setupSpinnerItemSelected(){
        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                val c = adapter.getItem(position) as LocalCountries
                val country = c.slug
                getDataByCountrySelected(country)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun getDataByCountrySelected(country: String){
        viewModel.loadingStateViewModel.showLoading()
        viewModel.getDataByCountry(country).observe(viewLifecycleOwner,{
            if(it.data != null){
                viewModel.loadingStateViewModel.hideLoading()
                setupDataByCountry(it.data)
            }else{
                viewModel.loadingStateViewModel.hideLoading()
                Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setupSpinnerDefaultItem(list: List<LocalCountries>){
        val items = list.sortedBy { it.country }
        adapter = get { parametersOf(items) }
        binding.spinner.adapter = adapter
        val pos = items.indexOfFirst { it.iSO2.equals(COUNTRY_CODE) } as Int
        binding.spinner.setSelection(pos)
    }

    private fun setupDataByCountry(country: List<Country>){
        if(country.size>0) {
            binding.countryistoday = country.last()
            binding.countryisyesterday = country.get(country.size - 2)
        }else{
            setupTextDefaultNoData()
        }
    }

    private fun setupTextDefaultNoData(){
        val initData = "No Data"
        binding.tvConfirmedTotal.text = initData
        binding.tvActiveTotal.text = initData
        binding.tvRecoveredTotal.text = initData
        binding.tvDeceasedTotal.text = initData

        binding.tvConfirmedNew.text = null
        binding.tvActiveNew.text = null
        binding.tvRecoveredNew.text = null
        binding.tvDeceasedNew.text = null

    }
}