package com.paiwad.myapp.covide_trackapp.di

import com.paiwad.myapp.covide_trackapp.data.Covid19Repository
import com.paiwad.myapp.covide_trackapp.data.source.Covid19RepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single<Covid19Repository> { Covid19RepositoryImpl(get(),get()) }
}