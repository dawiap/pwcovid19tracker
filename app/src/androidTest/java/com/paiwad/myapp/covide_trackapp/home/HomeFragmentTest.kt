package com.paiwad.myapp.covide_trackapp.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.paiwad.myapp.covide_trackapp.data.source.FakeAndroidRepository
import com.paiwad.myapp.covide_trackapp.presentation.MainActivity
import com.paiwad.myapp.covide_trackapp.presentation.fragment.HomeFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

@RunWith(AndroidJUnit4::class)
@MediumTest
@ExperimentalCoroutinesApi
class   HomeFragmentTest {

    private lateinit var repository: FakeAndroidRepository

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initRepository() {
        repository = FakeAndroidRepository()
    }

    @After
    fun reset() {
        //
    }


    @Test
    fun testNavigationToInHomeFragmentScreen() {
        val scenario = launchFragmentInContainer<HomeFragment>()
        val navController = mock(NavController::class.java)
        scenario.onFragment{fragment ->
            Navigation.setViewNavController(fragment.view!!, navController)
        }

        //then
    }
}
