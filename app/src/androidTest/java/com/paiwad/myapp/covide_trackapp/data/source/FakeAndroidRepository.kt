package com.paiwad.myapp.covide_trackapp.data.source

import com.example.myapplication.data.util.Resource
import com.paiwad.myapp.covide_trackapp.data.Covid19Repository
import com.paiwad.myapp.covide_trackapp.data.model.Country
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.model.SummaryResponse

class FakeAndroidRepository: Covid19Repository {
    private val countries = mutableListOf<LocalCountries>()

    private fun initCountries(){
        countries.add(LocalCountries("ALA Aland Islands", "ala-aland-islands", "AX"))
        countries.add(LocalCountries("Bahrain", "bahrain", "BM"))
        countries.add(LocalCountries("Cameroon", "cameroon", "CM"))
    }

    override suspend fun getDataSummary(): Resource<SummaryResponse> {
        TODO("Not yet implemented")
    }

    override suspend fun getDataByCountry(country: String): Resource<List<Country>> {
        TODO("Not yet implemented")
    }

    override suspend fun getCountries(isOnline: Boolean): Resource<List<LocalCountries>> {
        return Resource.Success(countries)
    }

}