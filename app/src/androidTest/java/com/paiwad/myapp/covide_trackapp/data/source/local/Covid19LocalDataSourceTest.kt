package com.paiwad.myapp.covide_trackapp.data.source.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth
import com.paiwad.myapp.covide_trackapp.data.db.CovidTrackingDatabase
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import com.paiwad.myapp.covide_trackapp.data.source.datasourceImpl.Covid19LocalDataSourceImpl
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class Covid19LocalDataSourceTest {

    private lateinit var localDataSource: Covid19LocalDataSourceImpl
    private lateinit var database: CovidTrackingDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(
                context,CovidTrackingDatabase::class.java
        ).build()

        localDataSource = Covid19LocalDataSourceImpl(dao = database.covid19Dao())
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun addCountriesToCatch_retievesCountries() = runBlocking {
        //give
        val countries = listOf(LocalCountries("ALA Aland Islands", "ala-aland-islands", "AX"))
        localDataSource.addCountryToCatch(countries)

        //when
        val result = localDataSource.getCountryFromCatch()

        //then
        Truth.assertThat(result).isNotEmpty()
        Truth.assertThat(result.size).isEqualTo(1)
        Truth.assertThat(result.last().country).isEqualTo("ALA Aland Islands")
        Truth.assertThat(result.last().iSO2).isEqualTo("ala-aland-islands")
        Truth.assertThat(result.last().slug).isEqualTo("AX")
    }

    @Test
    fun clearCountries_retievesEmpty() = runBlocking {
        //give
        val countries = listOf(LocalCountries("ALA Aland Islands", "ala-aland-islands", "AX"))
        localDataSource.addCountryToCatch(countries)

        //when
        localDataSource.clearCountry()
        val result = localDataSource.getCountryFromCatch()

        Truth.assertThat(result).isEmpty()
    }
}