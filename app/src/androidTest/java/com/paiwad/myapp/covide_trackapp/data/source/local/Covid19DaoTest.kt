package com.paiwad.myapp.covide_trackapp.data.source.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.paiwad.myapp.covide_trackapp.data.db.Covid19Dao
import com.paiwad.myapp.covide_trackapp.data.db.CovidTrackingDatabase
import com.paiwad.myapp.covide_trackapp.data.model.LocalCountries
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Covid19DaoTest {
    private lateinit var covid19Dao: Covid19Dao
    private lateinit var database: CovidTrackingDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(
            context,CovidTrackingDatabase::class.java
        ).build()

        covid19Dao = database.covid19Dao()
    }

    @After
    fun closeDb(){
        database.close()
    }

    @Test
    fun insetCountries_countryList_returnSuccess() = runBlocking {
        //give
        val countries = fakeCountries()

        //when
        covid19Dao.addCountry(countries)

        //then
        val allCountries = covid19Dao.getCountry()
        Truth.assertThat(allCountries).isEqualTo(allCountries)
    }

    @Test
    fun clearCountries_returnIsEmpty() = runBlocking{
        //give
        val countries = fakeCountries()
        covid19Dao.addCountry(countries)

        //when
        covid19Dao.clearCountry()

        //then
        val allCountries = covid19Dao.getCountry()
        Truth.assertThat(allCountries).isEmpty()
    }

    private fun fakeCountries(): List<LocalCountries>{
        val country = LocalCountries("thailand","TH","th")
        val countries = listOf<LocalCountries>(
            country,
            country.copy(country = "USA")
        )
        return countries
    }
}